# Jazz Plugin: Excel Extractor Documentation

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.


## Plugin Pipeline

### Tasks

#### extract

**Purpose**: _extract_ task is responsable for extracting data from excel spreadsheet and convert this into a javascript array of objects.

**Class**: ExtractorTask

**Parameters**:

- source: represents the path of the excel spreadsheet file. References to the inputParameter _sourceFile_.

**Ovewritables**:

- none

#### validate

> Note: this task is a Work in Progress. The _execute()_ method still do not validates anything.
> This will be very useful when you want to validate the excel data before moving to a next step in the pipeline.

**Purpose**: _validate_ task is responsable for validating the excel spreadsheet data.

**Class**: ExtractorTask

**Parameters**:

- none

**Ovewritables**:

- none

```javascript
export const pipeline = [
  {
    id: "extract",
    description: " Extracts excel data into javascript array of objects",
    class: "ExtractorTask",
    params: ["source"]
  },
  {
    id: "validate",
    description: " Validates extracted excel data based on plugin rules",
    class: "ValidateTask"
  }
];
```

### Input Parameters

```javascript
export const inputParameters = {
  sourceFile: {
    alias: "s",
    describe: "Source File",
    demandOption: true,
    default: "source.xlsx",
    name: "source"
  }
};
```

- sourceFile: indicates the path of the excel file from whch the process will extract the data.Default is the _source.xlsx_. References to the parameter named _source_ in the ExtractorTask Class.
