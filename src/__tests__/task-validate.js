/* eslint-disable import/no-extraneous-dependencies */
import { createConfigInfo } from 'jazz-core/dist/helpers';
import ValidateTask from '../ValidateTask';

const configFile = './src/__tests__/pack-config';
const config = createConfigInfo(configFile);
const task = new ValidateTask('validate', {}, config);
const data = ['row1', 'row2'];
task.setRawData(data);
task.execute(data);
