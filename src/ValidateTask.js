/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable class-methods-use-this */
import Task from './Task';
import { name as pluginName } from './config';

class ValidateTask extends Task {
  constructor(id, params, config, description = '', rawDataFrom = null) {
    super(id, params, config, description, rawDataFrom);
    [this.plugin] = this.config.plugins.filter(plugin => plugin.name === pluginName);
  }

  execute() {
    /** This is how you can have access to the data on specific pipeline step
     * const data = this.getPipeline().getResult("extract")
     * -> here we access the result from task named extract
     */

    this.logger.info('Executing validation task');
  }
}

module.exports = ValidateTask;
