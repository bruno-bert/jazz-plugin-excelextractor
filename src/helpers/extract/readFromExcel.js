const readFromExcel = (sourceFile, sheetName) => {
  const reader = require('xlsx');
  const workbook = reader.readFile(sourceFile);
  const data = reader.utils.sheet_to_json(workbook.Sheets[sheetName], { raw: true });
  return data;
};

module.exports = readFromExcel;
