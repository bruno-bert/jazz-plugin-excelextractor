export const name = 'jazz-plugin-excelextractor';
export const pipeline = [
  {
    id: 'extract',
    description: ' Extracts excel data into javascript array of objects',
    class: 'ExtractorTask',
    params: ['source'],
  },
  {
    id: 'validate',
    description: ' Validates extracted excel data based on plugin rules',
    class: 'ValidateTask',
  },
];

export const inputParameters = {
  sourceFile: {
    alias: 's',
    describe: 'Source File',
    demandOption: true,
    default: 'source.xlsx',
    name: 'source',
  },
};
