/* eslint-disable class-methods-use-this */
import Task from './Task';
import { readFromExcel, readFromText } from './helpers';
import { getFileExtension } from './Utils';
import { name as pluginName } from './config';

class ExtractorTask extends Task {
  constructor(id, params, config, description = '', rawDataFrom = null) {
    super(id, params, config, description, rawDataFrom);
    [this.plugin] = this.config.plugins.filter(plugin => plugin.name === pluginName);

    this.sourceFile = params.source;
    ExtractorTask.validateParameters(params);
  }

  execute() {
    const { sourceFile } = this;
    const extension = getFileExtension(sourceFile);
    const sheetName = this.plugin.sheet || 'source';
    let result = null;

    return new Promise((resolve, reject) => {
      if (extension === 'csv') {
        result = readFromText(sourceFile);
      } else {
        result = readFromExcel(sourceFile, sheetName);
      }

      resolve(result);
    });
  }

  static validateParameters({ source }) {
    if (!source) {
      throw new TypeError('Source file is required');
    }
  }
}
module.exports = ExtractorTask;
