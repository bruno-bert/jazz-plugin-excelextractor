/* eslint-disable import/no-extraneous-dependencies */
import { createConfigInfo } from 'jazz-core/dist/helpers';
import ExtractorTask from '../ExtractorTask';

const configFile = './src/__tests__/pack-config';
const config = createConfigInfo(configFile);
const task = new ExtractorTask(
  'extract',
  { source: '__e2e-tests__/source-files/source.xlsx' },
  config,
);
const data = ['row1', 'row2'];
task.setRawData(data);
task.execute(data);
